/*
 * Copyright 2018 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <UIKit/UIKit.h>
#import <FirebaseInAppMessaging/FIRInAppMessagingRendering.h>

#import "FIDModalViewController.h"
#import "FIRCore+InAppMessagingDisplay.h"


@interface FIDModalViewController ()


@property(nonatomic, readwrite) FIRInAppMessagingModalDisplay *modalDisplayMessage;

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) NSMutableArray<UIButton *> *buttons;

@end


@implementation FIDModalViewController

+ (FIDModalViewController *)instantiateViewControllerWithResourceBundle:(NSBundle *)resourceBundle displayMessage:(FIRInAppMessagingModalDisplay *)modalMessage displayDelegate:(id<FIRInAppMessagingDisplayDelegate>)displayDelegate timeFetcher:(id<FIDTimeFetcher>)timeFetcher
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"FIRInAppMessageDisplayStoryboard" bundle:resourceBundle];

    if (storyboard == nil)
    {
        FIRLogError(kFIRLoggerInAppMessagingDisplay, @"I-FID300001", @"Storyboard 'FIRInAppMessageDisplayStoryboard' not found in bundle %@", resourceBundle);
        return nil;
    }
    
    FIDModalViewController *modalVC = (FIDModalViewController *)[storyboard instantiateViewControllerWithIdentifier:@"modal-view-vc"];
    modalVC.displayDelegate = displayDelegate;
    modalVC.modalDisplayMessage = modalMessage;
    modalVC.timeFetcher = timeFetcher;
    
    modalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    modalVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    modalVC.buttons = [[NSMutableArray alloc] init];
    
    return modalVC;
}

- (void)closeButtonClicked:(id)sender
{
    [self dismissView:FIRInAppMessagingDismissTypeUserTapClose];
}

- (void)actionButtonTapped:(id)sender
{
    [self followActionURL];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = self.modalDisplayMessage.title;
    self.descLabel.text = self.modalDisplayMessage.bodyText;

    self.contentView.backgroundColor = self.modalDisplayMessage.displayBackgroundColor;
    self.titleLabel.textColor = self.modalDisplayMessage.textColor;
    self.descLabel.textColor = self.modalDisplayMessage.textColor;
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:[self iPadRegular] ? 50.0 : 36.0]];
    [self.descLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:[self iPadRegular] ? 25.0 : 18.0]];
    
    // Изображение
    if (self.modalDisplayMessage.imageData)
    {
        [self.imageView setImage:[UIImage imageWithData:self.modalDisplayMessage.imageData.imageRawData]];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    // Кнопки
    if (self.modalDisplayMessage.actionButton.buttonText.length != 0)
    {
        UIButton *button = [self buttonWithTitle:self.modalDisplayMessage.actionButton.buttonText andAction:@selector(actionButtonTapped:)];
//        self.actionButton.backgroundColor = self.modalDisplayMessage.actionButton.buttonBackgroundColor;
//        [self.actionButton setTitleColor:self.modalDisplayMessage.actionButton.buttonTextColor forState:UIControlStateNormal];
        [self.buttons addObject:button];
    }
    [self.buttons addObject:[self buttonWithTitle:@"Закрыть"/*TAXLocalizedString(@"button.close")*/ andAction:@selector(closeButtonClicked:)]];
    [self refreshButtons];
    
    if (self.modalDisplayMessage.renderAsTestMessage)
        FIRLogDebug(kFIRLoggerInAppMessagingDisplay, @"I-FID300011", @"Test message.");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // close any potential keyboard, which would conflict with the modal in-app messagine view
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    
    // Проверяем привязки
    [self checkScreenSize];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // Проверяем привязки
    [self checkScreenSize];
}

- (void)checkScreenSize
{
    // Тень модального окна
    if ([self iPadRegular])
    {
        self.shadowView.hidden = NO;
        self.bannerView.layer.cornerRadius = 15.0;
        self.shadowView.layer.cornerRadius = 15.0;
        self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.shadowView.layer.shadowOffset = CGSizeZero;
        self.shadowView.layer.shadowOpacity = 0.5;
        self.shadowView.layer.shadowRadius = 50.0;
        //self.bannerView.layer.borderColor = UIColor.black.cgColor;
        //self.bannerView.layer.borderWidth = 1.0 / UIScreen.main.scale;
    }
    else
    {
        self.shadowView.hidden = YES;
        self.bannerView.layer.cornerRadius = 0.0;
        //self.bannerView.layer.borderWidth = 0.0;
    }
    
    // Пропорции банера
    [self setTranslatesAutoresizingMaskIntoConstraints:NO forSubviews:self.contentView];
//    if (self.modalDisplayMessage.imageData)
//    {
//        UIImage *image = [UIImage imageWithData:self.modalDisplayMessage.imageData.imageRawData];
//        if ([self iPadRegular])
//        {
//            CGFloat ratio = image.size.width / image.size.height;
//            self.contentRatioConstraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:ratio constant:0.0];
//            [self.contentView addConstraint:self.contentRatioConstraint];
//        }
//    }
    
    [self.view layoutIfNeeded];
}

- (BOOL)iPadRegular
{
    // Узнаём размер экрана в зависимости от ориентации
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        CGSize windowSize = [[UIWindow alloc] init].bounds.size;
        BOOL iPadRegular = YES;
        if (screenSize.width > screenSize.height)
        {
            iPadRegular = windowSize.width > screenSize.width / 2; // - 10;
            return iPadRegular;
        }
        else
        {
            iPadRegular = windowSize.width == screenSize.width;
            return iPadRegular;
        }
    }
    return NO;
}

// Устанавливает еranslatesAutoresizingMask для всех родителей
- (void)setTranslatesAutoresizingMaskIntoConstraints:(BOOL)yesNO forSubviews:(UIView *)view
{
    if ([view isKindOfClass:[UIView class]])
        [view setTranslatesAutoresizingMaskIntoConstraints:yesNO];
    
    if ([view respondsToSelector:@selector(subviews)])
        for (id view_ in [view subviews])
            [self setTranslatesAutoresizingMaskIntoConstraints:yesNO forSubviews:view_];
}

#pragma mark - Buttona

// Оформление кнопки
- (UIButton *)buttonWithTitle:(NSString *)title andAction:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:18.0]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
    [button.layer setCornerRadius:3.0];
    return button;
}

// Выводит кнопки
- (void)refreshButtons
{
    // Удаляет текущие кнопки
    for (id button in [self.buttonsView subviews])
        [button removeFromSuperview];
    
    // Вертикальная привязка
    NSMutableArray *buttonsConstrainsArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *buttonsBindingsDictionary = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < [self.buttons count]; i++)
    {
        UIButton *button = [self.buttons objectAtIndex:i];
        [self.buttonsView addSubview:button];
        
        // Вертикальная привязка
        NSString *buttonKey = [NSString stringWithFormat:@"button%d", i];
        [buttonsBindingsDictionary setObject:button forKey:buttonKey];
        [buttonsConstrainsArray addObject:[NSString stringWithFormat:@"[%@(50.0)]", buttonKey]];
        
        // Горизонтальная привязка
        [self.buttonsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(button)]];
    }
    
    // Вертикальная привязка
    NSString *buttonsConstrains = [buttonsConstrainsArray componentsJoinedByString:@"-8.0-"];
    NSString *constrain = [NSString stringWithFormat:@"V:|%@|", buttonsConstrains];
    [self.buttonsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constrain options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:buttonsBindingsDictionary]];
}

@end
